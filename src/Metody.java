import java.util.Scanner;

public class Metody {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Wypisz pierwszą liczbę: ");
        int usersNumber1 = scan.nextInt();
        scan.nextLine();
        System.out.println("Wypisz drugą liczbę: ");
        int usersNumber2 = scan.nextInt();
        showResults(sumNumbers(usersNumber1, usersNumber2), (substractNumbers(usersNumber1, usersNumber2)));
    }

    static int sumNumbers(int x, int y) {
        return x + y;
    }

    static int substractNumbers(int x, int y) {
        return x - y;
    }

    static void showResults(int x, int y) {
        System.out.println("Liczba 1 + Liczba 2 = " + x);
        System.out.println("Liczba 1 - Liczba 2 = " + y);
    }
}
