public class Warunki {
    public static void main(String[] args) {
        double income = 3720;
        double tax;
        if (income < 2000) {
            System.out.println("Nie płacisz podatku!");
        } else if (income < 4000) {
            tax = income * 0.02;
            System.out.println("Podatek wynosi " + tax + "zł");
        } else {
            tax = income * 0.04;
            System.out.println("Podatek wynosi " + tax + "zł");
        }

        String dayOfWeek = "Sobota";

        switch (dayOfWeek) {
            case "Poniedziałek":
            case "Wtorek":
            case "Środa":
            case "Czwartek":
            case "Piątek":
                System.out.println("Pracujemy");
                break;
            case "Sobota":
            case "Niedziela":
                System.out.println("Wolne");
                break;
            default:
                System.out.println("Błędne dane.");
        }
    }
}