public class Tablice {
    public static void main(String[] args) {

        String[] products = new String[4];
        products[1] = "Komputer";
        products[2] = "Klawiatura";
        int[] money = new int[]{1, 2, 5, 10, 20, 50, 100, 200, 500};

        for (int i = 0; i < money.length; i++) {
            System.out.println(money[i]);
        }

        for (int i = 0; i < products.length; i++) {
            System.out.println(i + 1 + " " + products[i]);
            if (i == 2) {
                products[i] = "Myszka";
                System.out.println("Zmiana towaru, teraz " + (i + 1) + " to " + products[i]);
            }
        }

        String[][] twoDimArr = new String[3][4];
        twoDimArr[0][0] = "Lubię";
        twoDimArr[0][1] = "bardzo";
        twoDimArr[0][2] = "słodkie";
        twoDimArr[0][3] = "pieski";
        twoDimArr[1][0] = "Nie";
        twoDimArr[1][1] = "wiem";
        twoDimArr[1][2] = "co";
        twoDimArr[1][3] = "napisać";
        twoDimArr[2][0] = "Kolejny";
        twoDimArr[2][1] = "nijaki";
        twoDimArr[2][2] = "zmyślony";
        twoDimArr[2][3] = "tekst";

        for (int i = 0; i < twoDimArr.length; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(twoDimArr[i][j] + " ");
                if (j == 3) {
                    System.out.print("\b.\n");
                }
            }
        }

        int[][][] threeDimArr = new int[3][3][3];
        threeDimArr[0][0][0] = 1;
        threeDimArr[0][0][1] = 2;
        threeDimArr[0][0][2] = 3;
        threeDimArr[0][1][0] = 11;
        threeDimArr[0][1][1] = 12;
        threeDimArr[0][1][2] = 13;
        threeDimArr[0][2][0] = 21;
        threeDimArr[0][2][1] = 22;
        threeDimArr[0][2][2] = 23;
        threeDimArr[1][0][0] = 31;
        threeDimArr[1][0][1] = 32;
        threeDimArr[1][0][2] = 33;
        threeDimArr[1][1][0] = 41;
        threeDimArr[1][1][1] = 42;
        threeDimArr[1][1][2] = 43;
        threeDimArr[1][2][0] = 51;
        threeDimArr[1][2][1] = 52;
        threeDimArr[1][2][2] = 53;
        threeDimArr[2][0][0] = 61;
        threeDimArr[2][0][1] = 62;
        threeDimArr[2][0][2] = 63;
        threeDimArr[2][1][0] = 71;
        threeDimArr[2][1][1] = 72;
        threeDimArr[2][1][2] = 73;
        threeDimArr[2][2][0] = 81;
        threeDimArr[2][2][1] = 82;
        threeDimArr[2][2][2] = 83;

        long threeDimArrSum = 1;
        long threeDimArrSumBefore = 1;
        int countSteps = 1;
        for (int i = 0; i < threeDimArr.length; i++) {
            for (int j = 0; j < threeDimArr.length; j++) {
                for (int k = 0; k < threeDimArr.length; k++) {
                    threeDimArrSumBefore = threeDimArrSum;
                    threeDimArrSum += threeDimArr[i][j][k];
                    System.out.println(countSteps + ". " + threeDimArrSumBefore + "+" + threeDimArr[i][j][k] + " = " + threeDimArrSum);
                    countSteps++;
                }
            }
        }
    }
}
