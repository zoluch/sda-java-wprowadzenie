import java.util.ArrayList;
import java.util.List;

public class Listy {

    public static void main(String[] args) {
        List<Integer> myList = new ArrayList<Integer>();
        myList.add(3);
        myList.add(500);
        myList.add(400);

        for(Integer listElement:myList){
            System.out.println(listElement);
        }
        myList.remove(1);
        System.out.println("\nLista po usunięciu elementu");
        for(Integer listElement:myList){
            System.out.println(listElement);
        }
    }
}
