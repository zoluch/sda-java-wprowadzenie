import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WyrazeniaRegularne {
    public static void main(String[] args) {
        String ownPattern = "[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,3}";
        Pattern pattern = Pattern.compile(ownPattern);
        ;
        System.out.println("Podaj swój email: ");
        Scanner scan = new Scanner(System.in);
        String usersMail = scan.nextLine();
        Matcher matcher = pattern.matcher(usersMail);
        boolean matchFound = matcher.find();
        if (matchFound) {
            System.out.println("Twój email jest poprawny!");
        } else {
            System.out.println("Twój email jest niepoprawny!");
        }

        String streetPattern = "ul\\.[A-Za-z\\s]+\\d+(/\\d)*";
        Pattern patternCompiler = Pattern.compile(streetPattern);
        System.out.println("Podaj swój adres: ");
        String usersStreet = scan.nextLine();
        Matcher streetMatcher = patternCompiler.matcher(usersStreet);
        boolean streetMatcherFound = streetMatcher.find();
        if (streetMatcherFound) {
            System.out.println("Poprawny adres");
        } else {
            System.out.println("Niepoprawny adres");
        }

        String line = "Mam kredyt 3000 zl odsetki to 200 zl";
        String pattern2 = "[A-Za-z\\s]+(?<kredyt>\\d+)[A-Za-z\\s]+(?<odsetki>\\d+).*";

        Pattern r = Pattern.compile(pattern2);
        Matcher m = r.matcher(line);
        if (m.find()) {
            System.out.println("Kredyt: " + m.group("kredyt"));
            System.out.println("Odsetki: " + m.group("odsetki"));
        } else {
            System.out.println("Match not found");
        }
    }
}