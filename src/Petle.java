public class Petle {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println("Hello world! po raz " + i);
        }

        for (int i = 0; i <= 100; i++) {
            if (i % 2 == 0) {
                System.out.println("Parzysta " + i);
            } else {
                System.out.println("Nieparzysta " + i);
            }
        }

        String[] array = {"Ala", "ma", "kota"};
        for (String element : array) {
            System.out.print(element + " ");
        }
        System.out.println();

        int i = 0;
        while (i <= 100) {
            System.out.println(i);
            i++;
        }

        i = 10;
        do {
            System.out.println(i);
            ++i;
        } while (i < 10);

        i = 1;
        while (i < 100) {
            System.out.println(i);
            i++;
            if (i == 33) {
                break;
            }
        }

        for (i = 0; i < 10; i++) {
            if (i == 8) {
                continue;
            }
            System.out.println(i);
        }
    }
}
