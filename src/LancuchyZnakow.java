import java.util.Locale;
import java.util.Scanner;

public class LancuchyZnakow {
    public static void main(String[] args) {
        String text1 = "This is a test";
        String text2 = "This is a test";
        String text3 =new String("This is a test");
        System.out.println(text1==text2);
        System.out.println(text2==text3);
        System.out.println(text1==text3);
        System.out.println(text1.equals(text2));
        System.out.println(text2.equals(text3));
        System.out.println(text1.equals(text3));

        String newText = "My name is ";
        String newText2 = "John Doe";
        String finalText = newText + newText2;

        String newText3 = "This is ";
        String newText4 = "a test";
        String finalText2 = newText3.concat(newText4);

        System.out.println(finalText);
        System.out.println(finalText2);

        System.out.println("This is a test value".length());
        String testValue = "This is a test value";
        System.out.println(testValue.toUpperCase());
        System.out.println(testValue.toLowerCase());

        System.out.println(testValue.hashCode());
        System.out.println(testValue.indexOf("is"));

        String joke = "Hahahah! Funny joke!";
        System.out.print(joke.replaceAll("a", "o"));

        Scanner scan = new Scanner(System.in);
        System.out.println("\nWpisz swój tekst: ");
        String textLine = scan.nextLine();
        System.out.println(textLine);

        System.out.println("Wpisz swoją liczbę: ");
        int usersNumber = scan.nextInt();
        System.out.println(usersNumber);

        Scanner scanDoubleWithDot = new Scanner(System.in).useLocale(Locale.US);
        System.out.println("Double: ");
        double usersDouble = scanDoubleWithDot.nextDouble();
        System.out.println(usersDouble);

        scan.nextLine(); // Potrzebna linijka, zeby kolejny scan zadzialal

        System.out.println("Wpisz swój tekst: ");
        String usersText = scan.nextLine();
        System.out.println("Ilość znaków w tekście: " + usersText.length());
        String textWithoutSpaces = usersText.replaceAll(" ", "");
        int spacesInText = usersText.length() - textWithoutSpaces.length();
        System.out.println("Ilość spacji: " + spacesInText);

        int spaceNumber = 0;
        for(int i = 0; i < usersText.length(); i++) {
            char ch = usersText.charAt(i);
            if(ch == ' ') {
                spaceNumber++;
            }
        }
        System.out.println("Ilość spacji policzona w pętli: " + spaceNumber);
    }
}
