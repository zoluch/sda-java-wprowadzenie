import org.w3c.dom.ls.LSOutput;

import java.util.Locale;

public class ZmienneJava {
    static int myGlobalVariable = 99;
    static Integer myGlobalVariableInteger = 97;

    public static void main(String[] args) {
        byte zmiennaByte = 127;
        short zmiennaShort = 32767;
        float liczbaZmiennoPrzecinkowa = 12.1f;
        double liczbaTypuDouble = 12.1;
        System.out.println(zmiennaByte);
        System.out.println(zmiennaShort);
        System.out.println(liczbaZmiennoPrzecinkowa);
        System.out.println(liczbaTypuDouble);
        System.out.println(myGlobalVariable);
        myGlobalVariable = 100;
        final int secondGlobalVariable;
        secondGlobalVariable = 10;
        System.out.println(myGlobalVariable);
        System.out.println(secondGlobalVariable);
        innaMetoda();

        boolean myFalseValue = false;
        boolean myTrueValue = true;
        boolean myBooleanValue = myFalseValue && myTrueValue;
        System.out.println(myBooleanValue);
        char r='r';
        String mojTekst = "Tutaj jest mój tekst do sprawdzenia.";
        System.out.println(mojTekst);
        System.out.println(r);
    }


    public static void innaMetoda() {
        int innaZmienna = 0;
        System.out.println(innaZmienna);
        System.out.println(myGlobalVariable);
    }
}
